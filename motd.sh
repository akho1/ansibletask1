#!/bin/bash
ip_address=$(hostname -I | awk '{print $1}')
hostname=$(hostname)
os_info=$(grep PRETTY_NAME /etc/os-release | cut -d'"' -f2)


echo "IP Address: $ip_address"
echo "Hostname: $hostname"
echo "OS: $os_info"